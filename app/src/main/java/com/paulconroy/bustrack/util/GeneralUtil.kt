package com.paulconroy.bustrack.util

import android.app.Activity
import android.content.Context
import android.content.Context.INPUT_METHOD_SERVICE
import android.graphics.Typeface
import android.util.Log
import android.view.inputmethod.InputMethodManager
import com.paulconroy.bustrack.Constants
import java.lang.reflect.Type
import java.util.concurrent.TimeUnit
import javax.xml.datatype.DatatypeConstants.HOURS
import javax.xml.datatype.DatatypeConstants.DAYS




/**
 * General util class
 * Functions for general use throughout the app
 *
 * Created by paulconroy on 27/09/2017.
 */

class GeneralUtil {

    /**
     * Hides the keyboard if visible
     */
    fun hideKeyboard(ctx: Context) {
        val inputManager = ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        // check if no view has focus:
        val v = (ctx as Activity).currentFocus ?: return

        inputManager.hideSoftInputFromWindow(v.windowToken, 0)
    }

    /**
     * Returns the typeface with the custom font used throughout the application
     */
    fun getTypeface(ctx: Context, bold: Boolean): Typeface {
        when(bold) {
            true-> return Typeface.createFromAsset(ctx.assets, Constants().FONT_PATH_BOLD)
            else -> return Typeface.createFromAsset(ctx.assets, Constants().FONT_PATH)
        }
    }

    fun getMinuteToSeconds(minuteInt: Int): Int {
        val seconds = TimeUnit.MINUTES.toSeconds(minuteInt.toLong())
        Log.e("time", "seconds " + seconds)


        return seconds.toInt()
    }

    fun getSecondsToMinute(secondsInt: Long): Int {
        val minutes = TimeUnit.SECONDS.toMinutes(secondsInt)

        return minutes.toInt()
    }

    /**
     * Convert a millisecond duration to a string format
     *
     * @param millis A duration to convert to a string form
     * @return A string of the form "X Days Y Hours Z Minutes A Seconds".
     */
    fun getMilliSecondToMinute(millis: Long): Int {
        var millis = millis
        if (millis < 0) {
            throw IllegalArgumentException("Duration must be greater than zero!")
        }
        val minutes = TimeUnit.MILLISECONDS.toMinutes(millis)


        return minutes.toInt()
    }

    fun getMilliSecondToSecond(millis: Long): Int {
        var millis = millis
        if (millis < 0) {
            throw IllegalArgumentException("Duration must be greater than zero!")
        }
        val seconds = TimeUnit.MILLISECONDS.toSeconds(millis)


        return seconds.toInt()
    }

    fun getMinuteToMillisecond(minuteInt: Int): Long {
        return TimeUnit.MINUTES.toMillis(minuteInt.toLong())
    }
}
