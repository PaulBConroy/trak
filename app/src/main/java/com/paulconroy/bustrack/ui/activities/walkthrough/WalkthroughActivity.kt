package com.paulconroy.bustrack.ui.activities.walkthrough

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.FragmentActivity
import android.support.v4.content.ContextCompat
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.Toast
import com.paulconroy.bustrack.Constants

import com.paulconroy.bustrack.R
import com.paulconroy.bustrack.adapter.ViewPagerAdapter
import com.paulconroy.bustrack.adapter.RouteListAdapter
import com.paulconroy.bustrack.adapter.StopListAdapter
import com.paulconroy.bustrack.custom.WalkthroughViewPager
import com.paulconroy.bustrack.dialog.CustomNameDialog
import com.paulconroy.bustrack.dialog.LoadingDialog
import com.paulconroy.bustrack.interfaces.FragmentListener
import com.paulconroy.bustrack.interfaces.PagerListener
import com.paulconroy.bustrack.model.Favourite
import com.paulconroy.bustrack.model.Route
import com.paulconroy.bustrack.singleton.RouteTimer
import com.paulconroy.bustrack.transformer.ZoomPageTransformer
import com.paulconroy.bustrack.ui.activities.countdown.CountdownActivity
import com.paulconroy.bustrack.util.GeneralUtil
import io.realm.RealmResults

/**
 * Walk-Through Activity is a step-by-step process of find a route to track. The user is asked
 * to enter a bus stop, if successful, they will be asked to input direction they want to be heading
 * and finally to select the route (bus) from the list displayed to them.
 *
 * UI Logic is handled here.
 *
 * Created by paulconroy on 26/09/2017.
 */
class WalkthroughActivity : FragmentActivity(), ViewPager.OnPageChangeListener, PagerListener, WalkthroughView {

    lateinit var pager: WalkthroughViewPager
    lateinit var pagerAdapter: PagerAdapter

    lateinit var ivBreadTrailOne: ImageView
    lateinit var ivBreadTrailTwo: ImageView
    lateinit var ivBreadTrailThree: ImageView
    lateinit var adapter: StopListAdapter
    private lateinit var fragmentListener: FragmentListener
    private lateinit var dialog: LoadingDialog

    var routeListAdapter: RouteListAdapter? = null
    var stopListAdapter: StopListAdapter? = null

    private val breadTrailList = ArrayList<ImageView>()
    var busStopNumber: String = ""
    var selectedRoute = Route("", "", "", "", "", "")


    var presenter: WalkthroughPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_walkthrough)

        setUpPageTrails()
        pager = findViewById(R.id.vpPager) as WalkthroughViewPager
        pager.setPageTransformer(true, ZoomPageTransformer())
        dialog = LoadingDialog(this)

        pagerAdapter = ViewPagerAdapter(supportFragmentManager)
        pager.adapter = pagerAdapter
        pager.addOnPageChangeListener(this)

        if (intent.extras != null) {
            getPresenter().isFavourite = intent.getBooleanExtra(Constants().IS_FAVOURITE, false)
        }

    }

    /**
     * Sets the view pager's bread trail
     */
    private fun setUpPageTrails() {
        ivBreadTrailOne = findViewById(R.id.ivBreadTrailOne) as ImageView
        ivBreadTrailTwo = findViewById(R.id.ivBreadTrailTwo) as ImageView
        ivBreadTrailThree = findViewById(R.id.ivBreadTrailThree) as ImageView

        breadTrailList.add(ivBreadTrailOne)
        breadTrailList.add(ivBreadTrailTwo)
        breadTrailList.add(ivBreadTrailThree)

        ivBreadTrailOne.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.solid_circle))
    }

    /**
     * Returns an instance of the presenter implementation class to invoke business logic
     * functions
     */
    private fun getPresenter(): WalkthroughPresenterImp {
        if (presenter == null) {
            presenter = WalkthroughPresenterImp(this, this)
        }

        return presenter as WalkthroughPresenterImp
    }

    override fun onPageScrollStateChanged(state: Int) {
        //ignore
    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
        //ignore
    }

    override fun onPageSelected(position: Int) {
        selectBreadCrumb(position)
    }

    /**
     * Parse through each breadcrumb and use the solid circle drawable based on the position
     * of the viewpager visible to the user using the current viewpager element
     */
    private fun selectBreadCrumb(position: Int) {
        for ((i, breadcrumb: ImageView) in breadTrailList.withIndex()) {
            if (position == i) {
                breadcrumb.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.solid_circle))
            } else {
                breadcrumb.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.hallow_circle))
            }
        }
    }

    private fun checkRoutes(stopNumber: String) {
        getPresenter().getResults(stopNumber)
    }

    override fun getRoutes(): ArrayList<Route> {
        return getPresenter().routes
    }

    override fun proceed() {
        pager.currentItem = pager.currentItem + 1
    }

    /**
     * Hide the keyboard when visible
     */
    override fun hideKeyboard() {
        GeneralUtil().hideKeyboard(this)
    }

    override fun fetchResults() {
        checkRoutes(busStopNumber)
    }

    override fun setStopNumber(stopNumber: String) {
        busStopNumber = stopNumber
    }

    override fun getAdapter(): RouteListAdapter {
        if (routeListAdapter == null) {
            routeListAdapter = RouteListAdapter(this, getRoutes())
        } else {
            routeListAdapter!!.updateData(getDirectionRoutes())
        }
        return routeListAdapter as RouteListAdapter
    }

    override fun getFavouriteAdapter(): StopListAdapter {
        if (stopListAdapter == null) {
            stopListAdapter = StopListAdapter(this, this!!.getFavourites()!!)
        } else {
            stopListAdapter!!.updateData(this!!.getFavourites()!!)
        }
        return stopListAdapter as StopListAdapter
    }

    private fun getFavourites(): RealmResults<Favourite>? {
        return getPresenter().getFavourites(this)
    }

    override fun setListener(listener: FragmentListener) {
        fragmentListener = listener
    }

    override fun setDirection(direction: String) {
        getPresenter().direction = direction
    }

    override fun getDirectionRoutes(): ArrayList<Route> {
        return getPresenter().routesByDirection()
    }

    override fun displayDialog() {
        val loadingDialog = LoadingDialog(this)
        loadingDialog.showDialogRes()
    }

    override fun error() {
        Toast.makeText(this, "error", Toast.LENGTH_LONG).show()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        goBack()
    }

    override fun finish() {
        if (pager.currentItem == 0) {
            super.finish()
        }
    }

    override fun checkDirection(): String {
        return getPresenter().displayDirection()
    }

    override fun goBack() {
        if (pager.currentItem != 0) {
            pager.currentItem = getPresenter().goBackInPager(pager.currentItem)
        } else {
            this.finish()
        }
    }

    override fun alert(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    fun proceedToCountDown() {
        var intent = Intent(this, CountdownActivity::class.java)
        val routeTimer = RouteTimer.instance
        routeTimer.dueTime = selectedRoute.dueTime
        startActivity(intent)
        overridePendingTransition(R.anim.slide_in, R.anim.slide_out)
    }

    override fun addFavouriteToRealm(customName: String) {
        getPresenter().addStopFavourite(this, busStopNumber, customName)
    }

    override fun removeFavouriteFromRealm() {
        getPresenter().removeStopFavourite(this, busStopNumber)
    }

    override fun checkIfStopIsFavourite(): Boolean {
        return getPresenter().checkIfStopIsFavourite(this, busStopNumber)
    }

    override fun isFavourite(): Boolean {
        return getPresenter().isFavourite
    }

    override fun toggleSpinner(showSpinner: Boolean) {
        if (showSpinner) {
            dialog.showDialogRes()
        } else {
            dialog.dismissDialog()
        }
    }
}
