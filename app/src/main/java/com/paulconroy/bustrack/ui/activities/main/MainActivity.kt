package com.paulconroy.bustrack.ui.activities.main

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.crashlytics.android.Crashlytics
import com.paulconroy.bustrack.Constants
import com.paulconroy.bustrack.R
import com.paulconroy.bustrack.ui.activities.walkthrough.WalkthroughActivity
import com.paulconroy.bustrack.util.GeneralUtil
import io.fabric.sdk.android.Fabric



/**
 * Main Activity -> The main activity is essentially the splash screen that gives the
 * user two options, to either search for a route using a bus stop number or by selecting one
 * of their favourites.
 *
 * UI Logic is handled here.
 *
 * Created by paulconroy on 26/09/2017.
 */
class MainActivity : AppCompatActivity(), Animation.AnimationListener, View.OnClickListener, MainView{

    lateinit var ivLogoSplash: ImageView
    private lateinit var ivCirclePulseOne: ImageView
    lateinit var btnSearch: Button
    lateinit var btnFavourites: Button
    lateinit var tvTitle: TextView
    var presenter: MainPresenter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Fabric.with(this, Crashlytics())
        setContentView(R.layout.activity_main)

        ivLogoSplash = findViewById(R.id.ivLogoSplash) as ImageView
        ivCirclePulseOne = findViewById(R.id.ivCirclePulseOne) as ImageView
        tvTitle = findViewById(R.id.tvTitle) as TextView
        tvTitle.typeface = GeneralUtil().getTypeface(this, true)
        btnSearch = findViewById(R.id.btnSearch) as Button
        btnSearch.setOnClickListener(this)
        btnSearch.typeface = GeneralUtil().getTypeface(this, false)
        btnFavourites = findViewById(R.id.btnFavourites) as Button
        btnFavourites.setOnClickListener(this)
        btnFavourites.typeface = GeneralUtil().getTypeface(this, false)

        startPulse()
    }

    /**
     * Start the logo animation pulse
     */
    private fun startPulse() {
        val pulse: Animation = AnimationUtils.loadAnimation(this, R.anim.pulse)
        pulse.setAnimationListener(this)
        ivCirclePulseOne.animation = pulse
        pulse.start()
    }

    override fun onAnimationRepeat(animation: Animation?) {
        //ignore
    }

    override fun onAnimationEnd(animation: Animation?) {
        startPulse()
    }

    override fun onAnimationStart(animation: Animation?) {
        //ignore
    }

    fun toggleFavourites() {
        btnFavourites.isEnabled = getPresenter().checkAreFavourites(this)
    }

    /**
     * OnClick listener used to detect the search and favourites buttons
     * and invoke the correct function.
     *
     * If Search -> open the Search Walkthrough activity
     * If Favourites -> open the Favourites activity
     */
    override fun onClick(v: View?) {
        if (v is Button && v.getTag() == Constants().SEARCH_TAG) {
            startActivity(Intent(this, WalkthroughActivity::class.java))
            overridePendingTransition(R.anim.slide_in, R.anim.slide_out)
        } else {
            var intent = Intent(this, WalkthroughActivity::class.java)
            intent.putExtra(Constants().IS_FAVOURITE, true)
            startActivity(Intent(intent))
            overridePendingTransition(R.anim.slide_in, R.anim.slide_out)
        }
    }

    override fun onResume() {
        super.onResume()
        toggleFavourites()
    }

    private fun getPresenter(): MainPresenterImp {
        if (presenter == null) {
            presenter = MainPresenterImp(this, this)
        }

        return presenter as MainPresenterImp
    }
}
