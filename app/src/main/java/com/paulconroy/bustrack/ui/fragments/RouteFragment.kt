package com.paulconroy.bustrack.ui.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.TextView
import com.like.LikeButton
import com.like.OnLikeListener
import com.paulconroy.bustrack.R
import com.paulconroy.bustrack.dialog.CustomNameDialog
import com.paulconroy.bustrack.interfaces.FragmentListener
import com.paulconroy.bustrack.interfaces.PagerListener
import com.paulconroy.bustrack.util.GeneralUtil

/**
 * Route Fragment -> Uses will select a list of routes returned from the API call
 *
 * Created by paulconroy on 26/09/2017.
 */

class RouteFragment : Fragment(), FragmentListener, View.OnClickListener, OnLikeListener {

    lateinit var listener: PagerListener
    lateinit var rvRecyclerView: RecyclerView
    lateinit var ctx: Context
    lateinit var ivBack: ImageView
    lateinit var ivRoute: ImageView
    lateinit var lbFav: LikeButton
    lateinit var tvDescription: TextView
    lateinit var tvRouteTitle: TextView
    lateinit var tvDestinationTitle: TextView
    lateinit var tvDueTitle: TextView

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val rootView: ViewGroup = inflater?.inflate(R.layout.fragment_route, container, false) as ViewGroup
        listener.setListener(this)


        return rootView
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        rvRecyclerView = view?.findViewById(R.id.rvRecyclerView) as RecyclerView
        rvRecyclerView.layoutManager = LinearLayoutManager(ctx)

        ivBack = view?.findViewById(R.id.ivBack) as ImageView
        ivRoute = view?.findViewById(R.id.ivRoute) as ImageView
        tvRouteTitle = view?.findViewById(R.id.tvRouteTitle) as TextView
        tvDestinationTitle = view?.findViewById(R.id.tvDestinationTitle) as TextView
        tvDueTitle = view?.findViewById(R.id.tvDueTitle) as TextView
        tvDescription = view?.findViewById(R.id.tvDescription) as TextView
        lbFav = view?.findViewById(R.id.lbFav) as LikeButton

        lbFav.setOnLikeListener(this)
        lbFav.isLiked = listener.checkIfStopIsFavourite()

        tvRouteTitle.typeface = GeneralUtil().getTypeface(ctx, true)
        tvDestinationTitle.typeface = GeneralUtil().getTypeface(ctx, true)
        tvDueTitle.typeface = GeneralUtil().getTypeface(ctx, true)
        tvDescription.typeface = GeneralUtil().getTypeface(ctx, false)
        ivBack.setOnClickListener(this)

        val animation: Animation = AnimationUtils.loadAnimation(ctx, R.anim.subtle_bounce_with_fade)
        ivRoute.animation = animation
        animation.start()
        rvRecyclerView.adapter = listener.getAdapter()
        rvRecyclerView.adapter.notifyDataSetChanged()
    }

    /**
     * attach the pager listener to invoke methods in the walkthrough activity
     */
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener = context as PagerListener
        ctx = context
    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            rvRecyclerView.adapter = listener.getAdapter()
        } else {
            //Ignore -> For now
        }
    }

    override fun onClick(v: View?) {
        if (v is ImageView) {
            listener.goBack()
        }
    }

    override fun liked(p0: LikeButton?) {
        displayCustomNameDialog()
    }

    override fun unLiked(p0: LikeButton?) {
        listener.removeFavouriteFromRealm()
    }

    fun displayCustomNameDialog() {
        val customNameDialog: CustomNameDialog = CustomNameDialog(activity)
        customNameDialog.setOnClickListeners(View.OnClickListener {
            lbFav.isLiked = false
            customNameDialog.dismissDialog()})
        customNameDialog.showDialogRes()
    }
}
