package com.paulconroy.bustrack.ui.activities.walkthrough

/**
 * Walk-through Screen View Interface
 * Created by paulconroy on 27/09/2017.
 */
interface WalkthroughView {
    fun proceed()
    fun error()
    fun alert(message: String)
    fun toggleSpinner(showSpinner: Boolean)
}