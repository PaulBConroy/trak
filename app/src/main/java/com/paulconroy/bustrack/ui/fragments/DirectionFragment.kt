package com.paulconroy.bustrack.ui.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import com.paulconroy.bustrack.Constants
import com.paulconroy.bustrack.R
import com.paulconroy.bustrack.interfaces.PagerListener
import com.paulconroy.bustrack.util.GeneralUtil

/**
 * DirectionFragment -> Users will be asked to select direction of their route (Outbound or Inbound)
 * If the particular stop has only a single direction, one will be disabled.
 *
 * Created by paulconroy on 26/09/2017.
 */

class DirectionFragment : Fragment(), View.OnClickListener {

    lateinit var listener: PagerListener
    lateinit var btnOutbound: Button
    lateinit var btnInbound: Button
    lateinit var ivBack: ImageView
    lateinit var ivDirection: ImageView
    lateinit var tvDescription: TextView
    lateinit var ctx: Context

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView: ViewGroup = inflater?.inflate(R.layout.fragment_direction, container, false) as ViewGroup
        return rootView
    }

    /**
     * attach the pager listener to invoke methods in the walkthrough activity
     */
    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener = context as PagerListener
        this.ctx = context
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btnOutbound = view?.findViewById(R.id.tvOutbound) as Button
        btnOutbound.typeface = GeneralUtil().getTypeface(ctx, false)
        btnInbound = view.findViewById(R.id.tvInbound) as Button
        btnInbound.typeface = GeneralUtil().getTypeface(ctx, false)
        ivBack = view.findViewById(R.id.ivBack) as ImageView
        ivDirection = view.findViewById(R.id.ivDirection) as ImageView
        ivBack.setOnClickListener(this)
        tvDescription = view?.findViewById(R.id.tvDescription) as TextView
        tvDescription.typeface = GeneralUtil().getTypeface(ctx, false)

        val animation:Animation = AnimationUtils.loadAnimation(ctx, R.anim.subtle_bounce_with_fade)
        ivDirection.animation = animation
        animation.start()

        btnOutbound.setOnClickListener({ view -> onButtonClick(view as Button) })
        btnInbound.setOnClickListener({ view -> onButtonClick(view as Button) })
    }

    /**
     * Onclick method used for each direction button
     */
    private fun onButtonClick(view: Button) {
        if (view.text == (btnOutbound.text)) {
            listener.setDirection(btnOutbound.text.toString())
        } else {
            listener.setDirection(btnInbound.text.toString())
        }
        listener.proceed()
    }

    /**
     * When this view is visible, request check if stop is single or multi route
     */
    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            listener.getRoutes()
            enableDirections(listener.checkDirection())
        }
    }

    /**
     * Reset the directions and then check if the stop is a single or multi route stop.
     * If single, disable the direction not in use
     */
    private fun enableDirections(direction: String) {
        resetDirections()
        when(direction){
            Constants().OUTBOUND-> btnInbound.isEnabled = false
            Constants().INBOUND-> btnOutbound.isEnabled = false
        }
    }

    /**
     * Resets the directions and enables both
     */
    private fun resetDirections() {
        btnInbound.isEnabled = true
        btnOutbound.isEnabled = true
    }

    /**
     * Used for the back icon -> will take the user back a page in the view pager
     */
    override fun onClick(v: View?) {
        if (v is ImageView) {
            listener.goBack()
        }
    }
}
