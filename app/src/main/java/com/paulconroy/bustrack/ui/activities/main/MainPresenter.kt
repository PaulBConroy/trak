package com.paulconroy.bustrack.ui.activities.main

/**
 * Presenter Interface for Main screen
 *
 * Created by paulconroy on 25/09/2017.
 */
interface MainPresenter {

    /**
     * Checks if there are stops stored in Realm -> Returns a boolean
     */
    fun checkAreFavourites(activity: MainActivity): Boolean
}