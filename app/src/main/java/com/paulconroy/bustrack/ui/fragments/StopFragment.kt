package com.paulconroy.bustrack.ui.fragments

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.*
import com.paulconroy.bustrack.Constants
import com.paulconroy.bustrack.R
import com.paulconroy.bustrack.interfaces.PagerListener
import com.paulconroy.bustrack.util.GeneralUtil

/**
 * StopFragment -> Users will enter the bus stop number that will be used to query
 * the API.
 *
 * Created by paulconroy on 26/09/2017.
 */

class StopFragment : Fragment(), TextWatcher, View.OnClickListener{


    lateinit var etInput: EditText
    lateinit var ibSearch: ImageButton
    lateinit var listener: PagerListener
    lateinit var ivBack: ImageView
    lateinit var ivStop: ImageView
    lateinit var ivStopAlternative: ImageView
    lateinit var tvDescription: TextView
    lateinit var tvDescriptionFavourite: TextView
    lateinit var ctx: Context
    lateinit var rvRecyclerView: RecyclerView
    lateinit var rlFavouriteContainer: RelativeLayout
    lateinit var rlSearchContainer: RelativeLayout

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val rootView : ViewGroup = inflater?.inflate(R.layout.fragment_stop, container, false) as ViewGroup
        return rootView
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        tvDescription = view?.findViewById(R.id.tvDescription) as TextView
        tvDescriptionFavourite = view?.findViewById(R.id.tvDescriptionFavourite) as TextView
        tvDescription.typeface = GeneralUtil().getTypeface(ctx, false)
        tvDescriptionFavourite.typeface = GeneralUtil().getTypeface(ctx, false)
        ivBack = view?.findViewById(R.id.ivBack) as ImageView
        ivBack.setOnClickListener(this)
        ivStop = view?.findViewById(R.id.ivStop) as ImageView
        ivStopAlternative = view?.findViewById(R.id.ivStopAlternate) as ImageView
        rvRecyclerView = view?.findViewById(R.id.rvRecyclerView) as RecyclerView
        rvRecyclerView.layoutManager = LinearLayoutManager(ctx)
        rlFavouriteContainer = view?.findViewById(R.id.rlFavouriteContainer) as RelativeLayout
        rlSearchContainer = view?.findViewById(R.id.rlSearchContainer) as RelativeLayout

        val animation:Animation = AnimationUtils.loadAnimation(ctx, R.anim.subtle_bounce_with_fade)
        ivStop.animation = animation
        animation.start()

        etInput = view.findViewById(R.id.etInput) as EditText
        etInput.addTextChangedListener(this)
        etInput.typeface = GeneralUtil().getTypeface(ctx, false)

        ibSearch = view.findViewById(R.id.ibSearch) as ImageButton
        ibSearch.setOnClickListener(this)

        rvRecyclerView.adapter = listener.getFavouriteAdapter()
        rvRecyclerView.adapter.notifyDataSetChanged()

        if (listener.isFavourite()) {
            rlFavouriteContainer.visibility = View.VISIBLE
            val animation:Animation = AnimationUtils.loadAnimation(ctx, R.anim.subtle_bounce_with_fade)
            ivStopAlternative.animation = animation
            animation.start()
            rlSearchContainer.visibility = View.GONE
        }

    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        listener = context as PagerListener
        this.ctx = context
    }

    /**
     * Handle the process of search the routes and clearing edit text UI
     */
    private fun searchRoutes() {
        listener.hideKeyboard()
        listener.setStopNumber(etInput.text.toString())
        listener.fetchResults()
        etInput.text.clear()
    }

    override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
        if (etInput.length() == 4) {
            searchRoutes()
        }
    }

    override fun afterTextChanged(s: Editable?) {
        //ignore
    }

    override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        //ignore
    }

    override fun onClick(v: View?) {
        if (v!!.tag == Constants().BACK) {
            listener.goBack()
        } else {
            searchRoutes()
        }
    }
}
