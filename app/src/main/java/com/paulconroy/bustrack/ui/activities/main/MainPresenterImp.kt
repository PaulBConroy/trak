package com.paulconroy.bustrack.ui.activities.main

import android.content.Context
import com.paulconroy.bustrack.api.ApiClient
import com.paulconroy.bustrack.model.Results
import com.paulconroy.bustrack.realm.RealmController
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

/**
 * Main Presenter Implementation to handle business logic
 *
 * Created by paulconroy on 25/09/2017.
 */

class MainPresenterImp(view: MainView, context:Context) : MainPresenter {

    val view = view
    val context = context

    override fun checkAreFavourites(activity: MainActivity): Boolean {
        return RealmController(activity.application).checkAreFavourites()
    }

}