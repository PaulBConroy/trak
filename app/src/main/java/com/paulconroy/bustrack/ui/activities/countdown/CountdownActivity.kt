package com.paulconroy.bustrack.ui.activities.countdown

import android.animation.Animator
import android.animation.ObjectAnimator
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.util.Log
import android.view.View
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.Optional
import com.paulconroy.bustrack.Constants

import com.paulconroy.bustrack.R
import com.paulconroy.bustrack.custom.CircularTimerView
import com.paulconroy.bustrack.custom.TimeView
import com.paulconroy.bustrack.service.TimerService
import com.paulconroy.bustrack.singleton.RouteTimer
import com.paulconroy.bustrack.ui.activities.walkthrough.WalkthroughPresenterImp
import com.paulconroy.bustrack.util.GeneralUtil

/**
 * Countdown Activity -> Allows the user to select a partiuclar time until the app
 * should alert them. Also tracks how many minutes the bus is from the stop selected.
 *
 * UI Logic is handled here.
 *
 * Created by paulconroy on 28/09/2017.
 */
class CountdownActivity : AppCompatActivity(), View.OnClickListener, CountdownView {

    val ROTATION_Y = "rotationY"
    val ZERO_DEGREES = 0f
    val NINTY_DEGREES = 90f
    val ONE_EIGHTY_DEGREES = 180f
    val TWO_SEVENTY_DEGREES = 270f
    val THREE_SIXTY_DEGREES = 360f
    val HALF_SECOND = 300

    //Constant tags only in use in this activity
    val FIVE_TAG = "tvTime5"
    val TEN_TAG = "tvTime10"
    val FIFTEEN_TAG = "tvTime15"
    val TWENTY_TAG = "tvTime20"
    val TWENTY_FIVE_TAG = "tvTime25"
    val THIRTY_TAG = "tvTime30"
    val CANCEL_TAG = "ivCancel"

    val limitList = ArrayList<TimeView>()

    var isAnimating: Boolean = false
    var presenter: CountdownPresenter? = null
    lateinit var timerService: Intent

    lateinit var ivLogoSplash: ImageView
    lateinit var ivCancel: ImageView
    lateinit var tvTime5: TimeView
    lateinit var tvTime10: TimeView
    lateinit var tvTime15: TimeView
    lateinit var tvTime20: TimeView
    lateinit var tvTime25: TimeView
    lateinit var tvTime30: TimeView
    lateinit var tvDescription: TextView
    lateinit var tvInformation: TextView
    lateinit var tvMinutesRemain: TextView
    lateinit var ctvTimer: CircularTimerView
    lateinit var rlFront: RelativeLayout
    lateinit var rlBack: RelativeLayout
    lateinit var routeTimer: RouteTimer


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_countdown)
        ButterKnife.bind(this)

        ivLogoSplash = findViewById(R.id.ivLogoSplash) as ImageView
        ivCancel = findViewById(R.id.ivCancel) as ImageView
        ctvTimer = findViewById(R.id.ctvTimer) as CircularTimerView
        rlFront = findViewById(R.id.rlFront) as RelativeLayout
        rlBack = findViewById(R.id.rlBack) as RelativeLayout
        tvTime5 = findViewById(R.id.tvTime5) as TimeView
        tvTime10 = findViewById(R.id.tvTime10) as TimeView
        tvTime15 = findViewById(R.id.tvTime15) as TimeView
        tvTime20 = findViewById(R.id.tvTime20) as TimeView
        tvTime25 = findViewById(R.id.tvTime25) as TimeView
        tvTime30 = findViewById(R.id.tvTime30) as TimeView
        tvDescription = findViewById(R.id.tvDescription) as TextView
        tvInformation = findViewById(R.id.tvInformation) as TextView
        tvMinutesRemain = findViewById(R.id.tvMinutesRemain) as TextView
        tvMinutesRemain.typeface = GeneralUtil().getTypeface(this, true)
        tvDescription.typeface = GeneralUtil().getTypeface(this, false)
        ivCancel.setOnClickListener(this)

        tvTime5.tvTime.text = Constants().FIVE_MINUTES
        tvTime10.tvTime.text = Constants().TEN_MINUTES
        tvTime15.tvTime.text = Constants().FIFTEEN_MINUTES
        tvTime20.tvTime.text = Constants().TWENTY_MINUTES
        tvTime25.tvTime.text = Constants().TWENTY_FIVE_MINUTES
        tvTime30.tvTime.text = Constants().THIRTY_MINUTES

        limitList.add(tvTime5)
        limitList.add(tvTime10)
        limitList.add(tvTime15)
        limitList.add(tvTime20)
        limitList.add(tvTime25)
        limitList.add(tvTime30)

        routeTimer = RouteTimer.instance
        routeTimer.setActivity(this)

        timerService = Intent(this, TimerService::class.java)

        startAnimation()
        setTimeViewListeners()
        filterTimeViews()

    }

    override fun onClick(v: View?) {
        if (v is TimeView) {
            var limit = when(v.tag) {
                FIVE_TAG-> 5
                TEN_TAG-> 10
                FIFTEEN_TAG-> 15
                TWENTY_TAG-> 20
                TWENTY_FIVE_TAG-> 25
                THIRTY_TAG-> 30
                else -> 5
            }
            setTimer(limit)
            onTimeSetClick()
        }

        if (v is ImageView && v.tag == CANCEL_TAG) {
            cancelTimer()
            this.finish()
        }
    }

    private fun onTimeSetClick() {
        if (rlFront.visibility == View.VISIBLE && !isAnimating) {
            isAnimating = true
            val objectAnimator = ObjectAnimator.ofFloat(rlFront, ROTATION_Y, ZERO_DEGREES, NINTY_DEGREES)
            objectAnimator.duration = HALF_SECOND.toLong()
            objectAnimator.interpolator = AccelerateDecelerateInterpolator()
            objectAnimator.start()

            objectAnimator.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animator: Animator) {
                    //ignore
                }

                override fun onAnimationEnd(animator: Animator) {
                    rlBack.visibility = View.VISIBLE
                    rlBack.scaleX = -1f
                    val objectAnimator = ObjectAnimator.ofFloat(rlBack, ROTATION_Y, NINTY_DEGREES, ONE_EIGHTY_DEGREES)
                    objectAnimator.duration = HALF_SECOND.toLong()
                    objectAnimator.interpolator = AccelerateDecelerateInterpolator()
                    objectAnimator.start()
                    rlFront.visibility = View.GONE
                    objectAnimator.addListener(object : Animator.AnimatorListener {
                        override fun onAnimationStart(animator: Animator) {
                            //ignore
                        }

                        override fun onAnimationEnd(animator: Animator) {
                            isAnimating = false
                        }

                        override fun onAnimationCancel(animator: Animator) {
                            //ignore
                        }

                        override fun onAnimationRepeat(animator: Animator) {
                            //ignore
                        }
                    })
                }

                override fun onAnimationCancel(animator: Animator) {
                    //ignore
                }

                override fun onAnimationRepeat(animator: Animator) {}
            })
        }
        if (rlFront.visibility == View.GONE && !isAnimating) {
            isAnimating = true
            val objectAnimator = ObjectAnimator.ofFloat(rlBack, ROTATION_Y, ONE_EIGHTY_DEGREES, TWO_SEVENTY_DEGREES)
            objectAnimator.duration = HALF_SECOND.toLong()
            objectAnimator.interpolator = AccelerateDecelerateInterpolator()
            objectAnimator.start()

            objectAnimator.addListener(object : Animator.AnimatorListener {
                override fun onAnimationStart(animator: Animator) {
                    //ignore
                }

                override fun onAnimationEnd(animator: Animator) {
                    rlFront.visibility = View.VISIBLE
                    rlFront.scaleX = 1f
                    val objectAnimator = ObjectAnimator.ofFloat(rlFront, ROTATION_Y, TWO_SEVENTY_DEGREES, THREE_SIXTY_DEGREES)
                    objectAnimator.duration = HALF_SECOND.toLong()
                    objectAnimator.interpolator = AccelerateDecelerateInterpolator()
                    objectAnimator.start()
                    rlBack.visibility = View.GONE
                    objectAnimator.addListener(object : Animator.AnimatorListener {
                        override fun onAnimationStart(animator: Animator) {
                            //ignore
                        }

                        override fun onAnimationEnd(animator: Animator) {
                            isAnimating = false
                        }

                        override fun onAnimationCancel(animator: Animator) {
                            //ignore
                        }

                        override fun onAnimationRepeat(animator: Animator) {
                            //ignore
                        }
                    })
                }

                override fun onAnimationCancel(animator: Animator) {
                    //ignore
                }

                override fun onAnimationRepeat(animator: Animator) {
                    //ignore
                }
            })
        }

    }

    private fun getPresenter() : CountdownPresenterImp {
        if (presenter == null) {
            presenter = CountdownPresenterImp(this, this)
        }

        return presenter as CountdownPresenterImp
    }

    fun setTimer(totalSeconds: Int, remainingSeconds:Int) {
        ctvTimer.start(totalSeconds, remainingSeconds)
    }

    fun updateTimeText(minutesRemaining: Int) {
        tvMinutesRemain.text = minutesRemaining.toString()
    }

    fun setTimer(limit: Int) {
        routeTimer.alertTime = limit
        startService(timerService)
        displayInformation()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        cancelTimer()
    }

    override fun onResume() {
        super.onResume()
        if (routeTimer.hasStarted) {
            routeTimer.updateUITimer()
        }
    }

    fun cancelTimer() {
        stopService(timerService)
    }

    fun setTimeViewListeners() {
        for (timeView: TimeView in limitList) {
            timeView.setOnClickListener(this)
        }
    }

    fun filterTimeViews() {
        val dueTime = routeTimer.getDueTimeFormat().toInt()
        for (timeView: TimeView in limitList) {
            val time = timeView.tvTime.text.toString().toInt()
            if (dueTime <= time + 1) {
                timeView.alpha = .3f
                timeView.isClickable = false
            }
        }
    }

    fun startAnimation() {
        val animation:Animation = AnimationUtils.loadAnimation(this, R.anim.subtle_bounce_with_fade)
        ivLogoSplash.animation = animation
        animation.start()
    }

    fun displayInformation() {
        tvInformation.typeface = GeneralUtil().getTypeface(this, true)
        tvInformation.text = getString(R.string.information, routeTimer.alertTime.toString())
    }

}
