package com.paulconroy.bustrack.ui.activities.walkthrough

import com.paulconroy.bustrack.model.Favourite
import com.paulconroy.bustrack.model.Route
import io.realm.RealmResults

/**
 * Presenter Interface for Walk-through screen
 * Created by paulconroy on 27/09/2017.
 */
interface WalkthroughPresenter {

    /**
     * Fetch the result routes from the api
     */
    fun getResults(stopNumber : String)

    /**
     * return the result of routes based on the direction given
     */
    fun routesByDirection(): ArrayList<Route>

    /**
     * decrement through the locked viewpager
     */
    fun goBackInPager(position: Int) : Int

    /**
     * determine what direction option should be displayed to the user
     */
    fun displayDirection() : String

    /**
     * checks if the user has any favourites stored -> Returns a boolean
     */
    fun checkAreFavourites(activity: WalkthroughActivity): Boolean

    /**
     * adds the specified stop to their favourites
     */
    fun addStopFavourite(activity: WalkthroughActivity, stopNumber: String, customName: String)

    /**
     * removes the specified stop from their favourites
     */
    fun removeStopFavourite(activity: WalkthroughActivity, stopNumber: String)

    /**
     * checks if particular stop is already stored in favourites -> Returns a boolean
     */
    fun checkIfStopIsFavourite(activity: WalkthroughActivity, busStopNumber: String): Boolean

    /**
     * Gets a RealmResult list of favourites from Realm
     */
    fun getFavourites(activity: WalkthroughActivity): RealmResults<Favourite>?

}