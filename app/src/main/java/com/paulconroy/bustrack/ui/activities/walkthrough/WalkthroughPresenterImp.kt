package com.paulconroy.bustrack.ui.activities.walkthrough

import android.content.Context
import com.paulconroy.bustrack.Constants
import com.paulconroy.bustrack.api.ApiClient
import com.paulconroy.bustrack.dialog.CustomNameDialog
import com.paulconroy.bustrack.model.Favourite
import com.paulconroy.bustrack.model.Results
import com.paulconroy.bustrack.model.Route
import com.paulconroy.bustrack.realm.RealmController
import io.realm.RealmResults
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import timber.log.Timber

/**
 * Walk-through Presenter Implementation to handle business logic
 * Created by paulconroy on 27/09/2017.
 */

class WalkthroughPresenterImp(walkthroughActivity: WalkthroughActivity, context: Context) : WalkthroughPresenter, Callback<Results> {

    lateinit var results: Results
    private var view : WalkthroughActivity = walkthroughActivity
    private var context : Context = context
    var routes = ArrayList<Route>()
    var direction: String = ""
    var isFavourite: Boolean = false

    /**
     * Success response -> handle results accordingly and update the view UI
     */
    override fun onResponse(call: Call<Results>?, response: Response<Results>?) {
        Timber.d("Successful response")
        results = response!!.body()!!
        resultsToRoutes()
        view.toggleSpinner(false)
        if (!routes.isEmpty()) {
            view.proceed()
        } else {
            view.alert("Currently no routes")
        }
    }

    /**
     * Failure response -> Alert user that something went wrong
     */
    override fun onFailure(call: Call<Results>?, t: Throwable?) {
        //TODO Paul -> Handle failure
        Timber.e("Unsuccessful response")
        view.error()
        view.toggleSpinner(false)
    }

    override fun getResults(stopNumber: String) {
        view.toggleSpinner(true)
        ApiClient().apiController.getBus(stopNumber).enqueue(this)
    }

    private fun resultsToRoutes() : ArrayList<Route> {
        routes.clear()
        results.results!!
                .map {
                    Route(it.route!!, it.duetime!!, it.origin!!,
                            it.destination!!, it.arrivaldatetime!!, it.direction!!)
                }
                .forEach { routes.add(it) }
        return routes
    }

    override fun routesByDirection(): ArrayList<Route> {
        var directionRouteList = ArrayList<Route>()
        routes.filterTo(directionRouteList) { it.direction.toLowerCase().equals(direction.toLowerCase(), true) }
        return directionRouteList
    }

    override fun goBackInPager(position: Int): Int {
        if (position == 0) {
            return 0
        }

        return position - 1
    }

    override fun displayDirection() : String {
        var isOutBound = false
        var isInBound = false
        for (route in routes) {
            if (route.direction == "Outbound") {
                isOutBound = true
            } else {
                isInBound = true
            }
        }

        if (isInBound && isOutBound) {
            return Constants().MULTI
        }

        return if (isInBound) {
            Constants().INBOUND
        } else {
            Constants().OUTBOUND
        }
    }

    override fun checkAreFavourites(activity: WalkthroughActivity): Boolean {
        return RealmController(activity.application).checkAreFavourites()
    }

    override fun addStopFavourite(activity: WalkthroughActivity, stopNumber: String, customName: String) {
        RealmController(activity.application).addFavouriteStop(stopNumber, customName)
    }

    override fun removeStopFavourite(activity: WalkthroughActivity, stopNumber: String) {
        RealmController(activity.application).removeFavouriteStop(stopNumber)
    }

    override fun checkIfStopIsFavourite(activity: WalkthroughActivity, busStopNumber: String): Boolean {
        return RealmController(activity.application).checkIfStopIsFavourite(busStopNumber)
    }

    override fun getFavourites(activity: WalkthroughActivity): RealmResults<Favourite>? {
        return RealmController(activity.application).getFavourites()
    }
}