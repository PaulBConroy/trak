package com.paulconroy.bustrack.base

import android.support.v7.app.AppCompatActivity

/**
 * Base activity to which all activities will extend on.
 *
 * Created by paul conroy on 22/06/2017.
 */
abstract class BaseActivity<out P> : AppCompatActivity() {

    private var presenter: P? = null

    open fun getPresenter(): P? {
        return presenter
    }
}
