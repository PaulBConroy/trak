package com.paulconroy.bustrack.base

import android.content.Context
import com.paulconroy.bustrack.singleton.RouteTimer
import io.realm.BuildConfig
import io.realm.Realm
import io.realm.RealmConfiguration
import io.realm.RealmMigration

/**
 * Created by paulconroy on 13/10/2017.
 */

open class BaseModel {

    private var realm: Realm? = null

    /**
     * Creates a default Realm instance that can be used throughout the app without having
     * to create a new Realm instance every time.
     * <p/>
     * To use the default instance call the following:
     * Realm.getDefaultInstance();
     *
     * @param context        The Application Context
     * @param realmMigration Application specific Realm Model Migration
     * @param schemaVersion  The Realm Schema Version
     */
    fun initRealm(context: Context, realmMigration: RealmMigration, schemaVersion: Long) {
        Realm.init(context)

        var realmConfiguration : RealmConfiguration.Builder = RealmConfiguration
                .Builder()
                .name(BuildConfig.APPLICATION_ID)
                .schemaVersion(schemaVersion)
                .migration(realmMigration)

//        if (BuildConfig.DEBUG) {
//            realmConfiguration.deleteRealmIfMigrationNeeded()
//        }

        Realm.setDefaultConfiguration(realmConfiguration.build())
    }

    /**
     * @param realm Close the default Realm Instance
     */
    protected fun closeRealm(realm: Realm) {
        try {
            if (realm != null && !realm.isClosed) {
                realm.close()
            }
        } catch (ex: Exception) {
            //Log exception
        }
    }

    /**
     * @return The default Realm Instance
     */
    protected fun getRealm() : Realm {
        if (realm == null) {
            realm = Realm.getDefaultInstance()
        }

        return realm!!
    }

    /**
     * Destroys the Realm instance
     */
    protected fun onDestroy() {
        closeRealm(getRealm())
    }

}