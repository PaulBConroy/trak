package com.paulconroy.bustrack.singleton

import android.app.Activity
import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.graphics.Color
import android.os.Build
import android.os.CountDownTimer
import android.util.Log
import com.paulconroy.bustrack.R
import com.paulconroy.bustrack.ui.activities.countdown.CountdownActivity
import com.paulconroy.bustrack.util.GeneralUtil

/**
 * This singleton class will control the time keeping and notification alerts to the user.
 *
 * Created by paulconroy on 07/10/2017.
 */
class RouteTimer private constructor() {

    private var countDownTimer: CountDownTimer? = null
    private var activity: Activity? = null
    private val NOTIFICATION_ID: Int = 1
    var final: Boolean = false
    var notificationComplete: Boolean = false
    var hasStarted: Boolean = false
    var dueTime: String = ""
    var alertTime: Int = 0
    private lateinit var notificationManager: NotificationManager
    var milliSecondsRemaining: Long = 0

    companion object {
        val instance = RouteTimer()
    }

    fun startTimer() {
        if (countDownTimer == null) {
            var minutesToSeconds = GeneralUtil().getMinuteToSeconds(dueTime.toInt())
            setUITimer(minutesToSeconds, minutesToSeconds)
            hasStarted = true
            countDownTimer = object : CountDownTimer(GeneralUtil().getMinuteToMillisecond(dueTime.toInt()), 1000) {
                override fun onTick(millisUntilFinished: Long) {
                    milliSecondsRemaining = millisUntilFinished
                    if (!notificationComplete) {
                        updateNotification(GeneralUtil().getMilliSecondToMinute(millisUntilFinished))
                    }
                    (activity as CountdownActivity).updateTimeText(GeneralUtil().getMilliSecondToMinute(millisUntilFinished))
                }

                override fun onFinish() {
                    Log.e("Timer is now", "Done")
                    cancelTimer()
                }
            }.start()
        }
    }

    fun cancelTimer() {
        Log.e("Timer is now", "cancelled")
        if (countDownTimer != null) {
            notificationManager.cancelAll()
            countDownTimer!!.cancel()
            resetSettings()
        }
    }

    fun setActivity(activity: Activity) {
        this.activity = activity
    }

    private fun updateNotification(minutesRemaining: Int) {
        if (activity != null) {
            if (minutesRemaining <= alertTime) {
                final = true
            }
            makeNotification(minutesRemaining.toString())
        }
    }

    /**
     * Creates a sticky notification that displays minutes remaining
     * Once the minute reaches the chosen alert limit, remove sticky.
     */
    fun makeNotification(minutes: String) {

        val context: Context = activity!!.applicationContext
        val intent = Intent(context, CountdownActivity::class.java)

        val pendingIntent: PendingIntent = PendingIntent.getActivity(context,
                NOTIFICATION_ID, intent, PendingIntent.FLAG_UPDATE_CURRENT)

        val notificationBuilder: Notification.Builder = Notification.Builder(context)
                .setContentTitle(context.getString(R.string.app_name))
                .setContentText(context.getString(R.string.notification_message, minutes))
                .setContentIntent(pendingIntent)
                .setSmallIcon(R.drawable.ic_ploy_icon)
                .setLargeIcon(BitmapFactory.decodeResource(context.resources, R.drawable.ic_ploy_icon))

        var notification: Notification = if (Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
            notificationBuilder.build()
        } else {
            notificationBuilder.notification
        }

        if (!final) {
            notification.flags = Notification.FLAG_NO_CLEAR or Notification.FLAG_ONGOING_EVENT
        } else {
            val pattern = longArrayOf(0, 200, 500, 500, 200)
            notificationBuilder.setVibrate(pattern)
            notificationBuilder.setLights(Color.GREEN, 3000, 3000)
            notificationBuilder.setContentText(context.getString(R.string.notification_final, minutes))
            notificationComplete = true
        }

        notificationManager = context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        notificationManager.notify(NOTIFICATION_ID, notification)
    }

    /**
     * Update the UI and display the minutes remaining
     */
    fun setUITimer(totalSeconds: Int, remainingSeconds: Int) {
        (activity as CountdownActivity).setTimer(
                totalSeconds,
                remainingSeconds)
    }

    /**
     *
     */
    fun updateUITimer() {
        var totalSeconds = GeneralUtil().getMilliSecondToSecond(milliSecondsRemaining)
        (activity as CountdownActivity).setTimer(
                totalSeconds,
                totalSeconds)
    }

    /**
     * Resets the counter settings
     */
    fun resetSettings() {
        countDownTimer = null
        final = false
        notificationComplete = false
        hasStarted = false
    }

    fun getDueTimeFormat(): String {
        return when(dueTime) {
            "Due" -> "0"
            else -> dueTime
        }
    }
}
