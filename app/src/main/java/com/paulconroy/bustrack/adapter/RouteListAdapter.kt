package com.paulconroy.bustrack.adapter


import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import butterknife.ButterKnife
import butterknife.OnClick
import com.paulconroy.bustrack.R
import com.paulconroy.bustrack.model.Route
import com.paulconroy.bustrack.ui.activities.walkthrough.WalkthroughActivity
import com.paulconroy.bustrack.util.GeneralUtil

/**
 * Created by paulconroy on 28/09/2017.
 */

class RouteListAdapter(context: Context, data: ArrayList<Route>) : RecyclerView.Adapter<RouteListAdapter.ViewHolder>() {

    var data: ArrayList<Route> = data
    var ctx: Context = context

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent?.context).inflate(R.layout.route_item, parent, false)
        val viewHolder: RouteListAdapter.ViewHolder = RouteListAdapter.ViewHolder(view)
        return viewHolder
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {
        if (holder?.tvRoute != null) {
            holder?.tvRoute.text = data.get(position).route
            holder?.tvRoute.typeface = GeneralUtil().getTypeface(ctx, false)
        }

        if (holder?.tvDestination != null) {
            holder?.tvDestination.text = data[position].destination
            holder?.tvDestination.typeface = GeneralUtil().getTypeface(ctx, false)
        }

        if (holder?.tvDue != null) {
            holder?.tvDue.text = returnFormatTime(data[position].dueTime)
            holder?.tvDue.typeface = GeneralUtil().getTypeface(ctx, false)
        }

        if (holder?.route != null) {
            holder.route = data[position]
        }

        holder?.rlItem?.setOnClickListener({
            Log.e("clicked", "position" + data[position].arrivalTime)
            (ctx as WalkthroughActivity).selectedRoute = holder.route!!
            (ctx as WalkthroughActivity).proceedToCountDown()
        })
    }

    override fun getItemCount(): Int {
        if (this.data != null) {
            return this.data.size
        }
        return 0
    }

    fun updateData(data: ArrayList<Route>) {
        this.data = data
        notifyDataSetChanged()
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val tvRoute: TextView = itemView.findViewById(R.id.tvRoute) as TextView
        val tvDue: TextView = itemView.findViewById(R.id.tvDue) as TextView
        val tvDestination: TextView = itemView.findViewById(R.id.tvDestination) as TextView
        val rlItem: RelativeLayout = itemView.findViewById(R.id.rlItem) as RelativeLayout
        var route = Route("", "", "", "", "", "")

    }

    fun returnFormatTime(dueTime: String): String {
        return when(dueTime) {
            "Due"-> dueTime
            else -> dueTime + "mins"
        }
    }
}