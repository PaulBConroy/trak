package com.paulconroy.bustrack.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RelativeLayout
import android.widget.TextView
import com.paulconroy.bustrack.R
import com.paulconroy.bustrack.model.Favourite
import com.paulconroy.bustrack.model.Route
import com.paulconroy.bustrack.ui.activities.walkthrough.WalkthroughActivity
import com.paulconroy.bustrack.util.GeneralUtil
import io.realm.RealmResults

/**
 * Created by paulconroy on 28/09/2017.
 */

class StopListAdapter(context: Context, data: RealmResults<Favourite>) : RecyclerView.Adapter<StopListAdapter.ViewHolder>() {

    var data: RealmResults<Favourite> = data
    var ctx: Context = context

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        val view: View = LayoutInflater.from(parent?.context).inflate(R.layout.stop_item, parent, false)
        val viewHolder: StopListAdapter.ViewHolder = StopListAdapter.ViewHolder(view)
        return viewHolder
    }

    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {

        if (holder?.tvStop != null) {
            holder?.tvStop.text = data[position].stopNumber
            holder?.tvStop.typeface = GeneralUtil().getTypeface(ctx, false)
        }

        if (holder?.tvName != null) {
            holder?.tvName.text = data[position].customName
            holder?.tvName.typeface = GeneralUtil().getTypeface(ctx, false)
        }

        holder?.rlItem?.setOnClickListener({
            (ctx as WalkthroughActivity).setStopNumber(holder?.tvStop.text.toString())
            (ctx as WalkthroughActivity).fetchResults()
        })
    }

    override fun getItemCount(): Int {
        if (this.data != null) {
            return this.data.size
        }
        return 0
    }

    fun updateData(data: RealmResults<Favourite>) {
        this.data = data
        notifyDataSetChanged()
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val tvStop: TextView = itemView.findViewById(R.id.tvStop) as TextView
        val tvName: TextView = itemView.findViewById(R.id.tvName) as TextView
        val rlItem: RelativeLayout = itemView.findViewById(R.id.rlItem) as RelativeLayout

    }
}