package com.paulconroy.bustrack.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.paulconroy.bustrack.ui.fragments.DirectionFragment
import com.paulconroy.bustrack.ui.fragments.RouteFragment
import com.paulconroy.bustrack.ui.fragments.StopFragment

/**
 * Created by paulconroy on 26/09/2017.
 */

class ViewPagerAdapter(fragmentManager : FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {

    val NUM_PAGES: Int = 3
    var stopFragment = StopFragment()
    var directionFragment = DirectionFragment()
    var routeFragment = RouteFragment()

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> stopFragment
            1 -> directionFragment
            else -> routeFragment
        }
    }

    override fun getCount(): Int {
        return NUM_PAGES
    }


}
