package com.paulconroy.bustrack.dialog

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.Window
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import com.paulconroy.bustrack.R
import com.paulconroy.bustrack.ui.activities.walkthrough.WalkthroughActivity
import com.paulconroy.bustrack.util.GeneralUtil

/**
 * Created by paulconroy on 14/08/2017.
 */

class CustomNameDialog(activity: Activity) {

    private val activity: Activity = activity
    private val dialog: Dialog = Dialog(activity)
    private var tvTitle: TextView
    private var etName: EditText
    private var btnConfirm: Button
    private var btnCancel: Button
    private lateinit var cancelListener: View.OnClickListener

    init {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_custom)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window.attributes.windowAnimations = R.style.DialogAnimation

        tvTitle = dialog.findViewById(R.id.tvTitle) as TextView
        etName = dialog.findViewById(R.id.etName) as EditText
        btnConfirm = dialog.findViewById(R.id.btnConfirm) as Button
        btnCancel = dialog.findViewById(R.id.btnCancel) as Button
    }

    fun showDialogRes() {
        if (tvTitle != null) {
            tvTitle.typeface = GeneralUtil().getTypeface(activity.applicationContext, true)
        }

        if (btnCancel != null) {
            btnCancel.setOnClickListener(cancelListener)
        }

        if (btnConfirm != null) {
            btnConfirm.setOnClickListener(View.OnClickListener { addFavourite() })
        }

        dialog.show()
    }

    fun dismissDialog() {
        dialog.dismiss()
    }

    fun setOnClickListeners(cancelListener: View.OnClickListener) {
        this.cancelListener = cancelListener
    }

    private fun addFavourite() {
        ((activity) as WalkthroughActivity).addFavouriteToRealm(etName.text.toString())
        dismissDialog()
    }

}