package com.paulconroy.bustrack.dialog

import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.View
import android.view.Window
import android.widget.TextView
import com.paulconroy.bustrack.R
import com.paulconroy.bustrack.util.GeneralUtil

/**
 * Created by paulconroy on 14/08/2017.
 */

class LoadingDialog(activity: Activity) {

    private val activity: Activity = activity
    private val dialog: Dialog = Dialog(activity)
    private var tvTitle: TextView

    init {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setCancelable(false)
        dialog.setContentView(R.layout.dialog_loading)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window.attributes.windowAnimations = R.style.DialogAnimation

        tvTitle = dialog.findViewById(R.id.tvTitle) as TextView

    }

    fun showDialogRes() {
        if (tvTitle != null) {
            tvTitle.typeface = GeneralUtil().getTypeface(activity.applicationContext, true)
        }

        dialog.show()
    }

    fun dismissDialog() {
        dialog.dismiss()
    }

}
