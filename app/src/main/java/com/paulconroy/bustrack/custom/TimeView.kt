package com.paulconroy.bustrack.custom

import android.content.Context
import android.util.AttributeSet
import android.widget.TextView
import android.view.LayoutInflater
import android.widget.RelativeLayout
import com.paulconroy.bustrack.R


/**
 * Created by paulconroy on 06/10/2017.
 */
class TimeView : RelativeLayout {

    internal var mInflater: LayoutInflater
    lateinit var tvTime: TextView

    constructor(context: Context) : super(context) {
        mInflater = LayoutInflater.from(context)
        init()

    }

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle) {
        mInflater = LayoutInflater.from(context)
        init()
    }

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        mInflater = LayoutInflater.from(context)
        init()
    }

    fun init() {
        val v = mInflater.inflate(R.layout.time_item, this, true)
        tvTime = v.findViewById(R.id.tvTime) as TextView
    }
}