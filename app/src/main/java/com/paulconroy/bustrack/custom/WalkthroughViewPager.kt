package com.paulconroy.bustrack.custom

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent

/**
 * Created by paulconroy on 26/09/2017.
 */

class WalkthroughViewPager : ViewPager {

    private var isLocked: Boolean = true

    constructor(ctx: Context) : super(ctx)

    constructor(ctx: Context, attrs: AttributeSet) : super(ctx, attrs)

    override fun onTouchEvent(ev: MotionEvent?): Boolean {
        if (!isLocked) {
            return super.onTouchEvent(ev)
        }
        return false
    }

    override fun onInterceptTouchEvent(ev: MotionEvent?): Boolean {
        if (!isLocked) {
            return super.onInterceptTouchEvent(ev)
        }
        return false
    }

    fun setLock(isLocked: Boolean) {
        this.isLocked = isLocked
    }
}