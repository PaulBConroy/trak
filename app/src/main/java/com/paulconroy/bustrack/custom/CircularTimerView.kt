package com.paulconroy.bustrack.custom

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.View
import android.view.animation.LinearInterpolator
import com.paulconroy.bustrack.R
import java.util.concurrent.TimeUnit

/**
 * @author paulconroy
 * @since 21/07/2016
 */
class CircularTimerView

/**
 * constructor
 *
 * @param context      the context
 * @param attrs        attributes
 * @param defStyleAttr default style
 */
@JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyleAttr: Int = 0) : View(context, attrs, defStyleAttr) {

    private var bitmap: Bitmap? = null
    private var canvas: Canvas? = null

    private var circleOuterBounds: RectF? = null
    private var circleInnerBounds: RectF? = null

    private val backgroundCirclePaint: Paint
    private val circlePaint: Paint
    private val eraserPaint: Paint

    private var circleSweepAngle: Float = 0.toFloat()

    private var timerAnimator: ValueAnimator? = null

    init {

        var circleColor = Color.RED

        if (attrs != null) {
            val ta = context.obtainStyledAttributes(attrs, R.styleable.CircularTimerView)
            try {
                circleColor = ta.getColor(R.styleable.CircularTimerView_circleColor, circleColor)
            } finally {
                ta.recycle()
            }
        }

        backgroundCirclePaint = Paint()
        backgroundCirclePaint.isAntiAlias = true
        backgroundCirclePaint.color = ContextCompat.getColor(getContext(), R.color.mainColor)

        circlePaint = Paint()
        circlePaint.isAntiAlias = true
        circlePaint.color = circleColor

        eraserPaint = Paint()
        eraserPaint.isAntiAlias = true
        eraserPaint.color = Color.TRANSPARENT
        eraserPaint.xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        if (w != oldw || h != oldh) {
            bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
            canvas = Canvas(bitmap!!)
        }

        super.onSizeChanged(w, h, oldw, oldh)
        updateBounds()
    }

    /**
     * draw onto canvas
     *
     * @param canvas the canvas object
     */
    override fun onDraw(canvas: Canvas) {
        this.canvas!!.drawColor(0, PorterDuff.Mode.CLEAR)

        if (circleSweepAngle > 0f) {
            //Draw the Background Circle in Grey
            this.canvas!!.drawArc(circleOuterBounds!!, 0f, ARC_FULL.toFloat(), true, backgroundCirclePaint)

            //Draw the Progress Circle
            this.canvas!!.drawArc(circleOuterBounds!!, ARC_START_ANGLE.toFloat(), circleSweepAngle, true, circlePaint)
            this.canvas!!.drawOval(circleInnerBounds!!, eraserPaint)
        }

        canvas.drawBitmap(bitmap!!, 0f, 0f, null)
    }

    /**
     * Start the animation with the number of seconds
     *
     * @param totalSeconds     Integer number of total seconds of countdown
     * @param secondsRemaining Integer number of seconds remaining
     */
    fun start(totalSeconds: Int, secondsRemaining: Int) {
        val offset = (totalSeconds - secondsRemaining) / 10.0f

        timerAnimator = ValueAnimator.ofFloat(offset, 1f)
        timerAnimator!!.duration = TimeUnit.SECONDS.toMillis(secondsRemaining.toLong())
        timerAnimator!!.interpolator = LinearInterpolator()
        timerAnimator!!.addUpdateListener { animation -> drawProgress(animation.animatedValue as Float) }

        timerAnimator!!.start()
    }

    /**
     * Set the Angle sweep value and force the view to redraw
     *
     * @param progress the progress float
     */
    private fun drawProgress(progress: Float) {
        circleSweepAngle = 360 * progress
        invalidate()
    }

    private fun updateBounds() {
        val thickness = width * THICKNESS_SCALE

        circleOuterBounds = RectF(0f, 0f, width.toFloat(), height.toFloat())
        circleInnerBounds = RectF(
                circleOuterBounds!!.left + thickness,
                circleOuterBounds!!.top + thickness,
                circleOuterBounds!!.right - thickness,
                circleOuterBounds!!.bottom - thickness)

        invalidate()
    }

    companion object {

        private val ARC_START_ANGLE = 270
        private val ARC_FULL = 360

        private val THICKNESS_SCALE = 0.03f
    }
}
