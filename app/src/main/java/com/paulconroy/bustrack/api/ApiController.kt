package com.paulconroy.bustrack.api

import com.paulconroy.bustrack.model.Bus
import com.paulconroy.bustrack.model.Result
import com.paulconroy.bustrack.model.Results
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by paulconroy on 25/09/2017.
 */
interface ApiController {

    /**
     * Fetch the results for a particular stop id
     */
    @GET("realtimebusinformation") // http://www.dublinked.ie/cgi-bin/rtpi/realtimebusinformation?
    fun getBus(@Query("stopid") query: String) : Call<Results>

    /**
     * Fetch the initial data on all Dublin bus stops.
     * Used on app set-up
     */
    @GET("realtimebusinformation")
    fun getInitialStopData(@Query("stopid&format=json") query: String) : Call<Results>
}