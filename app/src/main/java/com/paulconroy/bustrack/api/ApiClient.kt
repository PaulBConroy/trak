package com.paulconroy.bustrack.api

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.paulconroy.bustrack.Constants
import com.paulconroy.bustrack.model.Bus
import com.paulconroy.bustrack.model.Result
import com.paulconroy.bustrack.model.Results
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber

/**
 * Created by paulconroy on 24/09/2017.
 */

class ApiClient : Callback<Results> {

    lateinit var results: Results
    lateinit var apiController: ApiController

    override fun onResponse(call: Call<Results>?, response: Response<Results>?) {
        Timber.d("Successful response")
        results = response!!.body()!!
    }

    override fun onFailure(call: Call<Results>?, t: Throwable?) {
       //TODO Paul -> Handle failure
        Timber.e("Unsuccessful response")
    }

    init {
        start()
    }

    fun start() {
        val gson:Gson = GsonBuilder().setLenient().create()

        val retrofit:Retrofit = Retrofit.Builder()
                .baseUrl(Constants().BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()

        apiController = retrofit.create(ApiController::class.java)


    }

    fun getController( stopNumber:String) : ApiController {
        return this.apiController
    }
}