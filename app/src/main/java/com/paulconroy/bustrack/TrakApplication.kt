package com.paulconroy.bustrack

import android.app.Application
import com.paulconroy.bustrack.base.BaseModel
import com.paulconroy.bustrack.realm.TrakRealmMigration

/**
 * Created by paulconroy on 13/10/2017.
 */

class TrakApplication: Application() {

    override fun onCreate() {
        super.onCreate()

        BaseModel().initRealm(applicationContext, TrakRealmMigration(), Constants().REALM_SCHEME_VERSION)
    }
}