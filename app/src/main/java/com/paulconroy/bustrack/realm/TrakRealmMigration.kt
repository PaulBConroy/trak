package com.paulconroy.bustrack.realm

import android.util.Log
import io.realm.DynamicRealm
import io.realm.RealmMigration

/**
 * TrakRealmMigration class to implement RealmMigration.
 *
 * Created by paulconroy on 13/10/2017.
 */

class TrakRealmMigration : RealmMigration {

    private val VERSION_0 = "0"

    override fun migrate(realm: DynamicRealm, oldVersion: Long, newVersion: Long) {
        //RealmSchema used to add or delete fields as needed.
        val schema = realm.schema

        //No break in switch is intentional.
        //Allow it to fall through
        when (oldVersion.toString()) {
            VERSION_0 -> {
            }
            else -> Log.e("default_migration", "Migration default case reached. Realm schema version not found")
        }
    }

}