package com.paulconroy.bustrack.realm

import android.app.Activity
import android.app.Application
import android.support.v4.app.Fragment
import com.paulconroy.bustrack.base.BaseModel
import com.paulconroy.bustrack.model.Favourite
import io.realm.RealmResults

/**
 * Created by paulconroy on 13/10/2017.
 */

class RealmController(application: Application) : BaseModel() {

    private lateinit var instance: RealmController

    fun with(activity: Activity): RealmController {
        if (instance == null) {
            instance = RealmController(activity.application)
        }

        return instance
    }

    fun with(fragment: Fragment): RealmController {
        if (instance == null) {
            instance = RealmController(fragment.activity.application)
        }

        return instance
    }

    fun getInstance(): RealmController {
        return instance
    }

    fun refresh() {
        getRealm().refresh()
    }

    private fun getFavourite(stopNumber: String): Favourite? {
        var favouriteQuery: Favourite? = null

        if (!getRealm().where(Favourite::class.java).findAll().isEmpty()) {
            val favourites: RealmResults<Favourite>? = getRealm().where(Favourite::class.java).findAll()
            favourites?.filter { it.stopNumber == stopNumber }?.forEach { favouriteQuery = it }
        }
        return favouriteQuery
    }

    fun getFavourites(): RealmResults<Favourite>? {
        return getRealm().where(Favourite::class.java).findAll()
    }

    fun checkAreFavourites(): Boolean {
        var areFavourites = false

        if (!getRealm().where(Favourite::class.java).findAll().isEmpty()) {
            areFavourites = true
        }

        return areFavourites
    }

    fun checkIfStopIsFavourite(stopNumber: String): Boolean {
        var isFound = false
        if (checkAreFavourites()) {
            var favourites: RealmResults<Favourite>? = getRealm().where(Favourite::class.java).findAll()
            if (favourites != null) {
                favourites
                        .filter { it.stopNumber == stopNumber }
                        .forEach { isFound = true }
            }
        }
        return isFound
    }

    fun addFavouriteStop(stopNumber: String, customName: String) {
        if (!getRealm().isInTransaction) {
            getRealm().executeTransaction { realm ->
                var maxId: Number = 0
                if (checkAreFavourites()) {
                    maxId = getRealm().where(Favourite::class.java).max("id")
                }
                var favourite = getRealm().createObject(Favourite::class.java, maxId.toInt() + 1)
                favourite.stopNumber = stopNumber
                favourite.customName = customName

                realm.copyToRealmOrUpdate(favourite)
            }
        }
    }

    fun removeFavouriteStop(stopNumber: String) {
        if (!getRealm().isInTransaction) {
            getRealm().executeTransaction {
                if (checkAreFavourites()) {
                    val favourite: Favourite? = getFavourite(stopNumber)
                    favourite?.deleteFromRealm()
                }
            }
        }
    }
}