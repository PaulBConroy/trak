package com.paulconroy.bustrack.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

import javax.xml.transform.Result

class Bus {

    @SerializedName("errorcode")
    @Expose
    var errorcode: String? = null

    @SerializedName("errormessage")
    @Expose
    var errormessage: String? = null

    @SerializedName("numberofresults")
    @Expose
    var numberofresults: Int = 0

    @SerializedName("stopid")
    @Expose
    var stopid: String? = null

    @SerializedName("timestamp")
    @Expose
    var timestamp: String? = null

    @SerializedName("results")
    @Expose
    var results: List<Result>? = null

}
