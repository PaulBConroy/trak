package com.paulconroy.bustrack.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/**
 * Created by paulconroy on 25/09/2017.
 */

class Results {

    @SerializedName("results")
    @Expose
    var results:List<Result>? = null
}