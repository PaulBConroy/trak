package com.paulconroy.bustrack.model

/**
 * Created by paulconroy on 27/09/2017.
 */

class Route (route: String, dueTime: String, origin: String,
             destination: String, arrivalTime: String, direction: String) {

    var route: String = route
    var dueTime: String = dueTime
    var origin: String = origin
    var destination: String = destination
    var arrivalTime: String = arrivalTime
    var direction: String = direction

    init {

    }

}