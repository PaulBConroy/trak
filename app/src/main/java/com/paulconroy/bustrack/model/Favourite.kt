package com.paulconroy.bustrack.model

import io.realm.RealmObject
import io.realm.annotations.Index
import io.realm.annotations.PrimaryKey

/**
 * Favourite class stores the selected Stop Number to save for the user via Realm
 * Created by paulconroy on 13/10/2017.
 */

open class Favourite: RealmObject() {

    @PrimaryKey
    var id: Int = 0
    var stopNumber = ""
    var customName = ""

}