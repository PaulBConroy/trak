package com.paulconroy.bustrack

/**
 * Constant values used throughout the app are stored here
 * Created by paulconroy on 25/09/2017.
 */

class Constants {

    /**
     * Constant string format of base url
     */
    val BASE_URL: String = "http://www.dublinked.ie/cgi-bin/rtpi/"

    /**
     * Constant string format of realtime url
     */
    val REALTIME_URL = "realtimebusinformation"

    /**
     * Constant string format of Outbound direction
     */
    val OUTBOUND = "OUTBOUND"

    /**
     * Constant string format of Inbound direction
     */
    val INBOUND = "INBOUND"

    /**
     * Constant string format of Multi direction
     */
    val MULTI = "MULTI"

    /**
     * Constant string format of the search button tag used on main page
     */
    val SEARCH_TAG = "btnSearch"

    /**
     * Constant path of the font used within the application
     */
    val FONT_PATH = "fonts/ubuntu_regular.ttf"

    /**
     * Constant path of the bold font used within the application
     */
    val FONT_PATH_BOLD = "fonts/ubuntu_bold.ttf"

    /**
     * Constant string value of 5 minutes
     */
    val FIVE_MINUTES = "5"

    /**
     * Constant string value of 10 minutes
     */
    val TEN_MINUTES = "10"

    /**
     * Constant string value of 15 minutes
     */
    val FIFTEEN_MINUTES = "15"

    /**
     * Constant string value of 20 minutes
     */
    val TWENTY_MINUTES = "20"

    /**
     * Constant string value of 25 minutes
     */
    val TWENTY_FIVE_MINUTES = "25"

    /**
     * Constant string value of 30 minutes
     */
    val THIRTY_MINUTES = "30"

    /**
     * Constant Int value of the Realm Schema version code.
     */
    val REALM_SCHEME_VERSION: Long = 3

    /**
     * Constant String value of isFavourite used for intent extra bundle
     */
    val IS_FAVOURITE: String = "isFavourite"

    /**
     * Constant String used to determine tag for OnClick call in Stop Fragement
     */
    val BACK = "back"

}