package com.paulconroy.bustrack.service

import android.app.Service
import android.content.Intent
import android.os.IBinder
import com.paulconroy.bustrack.singleton.RouteTimer

/**
 * TimerService will control the Route Timer, this will handle if the notification is to be removed
 * should the application be killed or force closed.
 *
 * Created by paulconroy on 17/10/2017.
 */

class TimerService: Service() {

    val routeTimer:RouteTimer = RouteTimer.instance

    override fun onBind(intent: Intent?): IBinder {
        TODO("not implemented")
    }

    override fun onTaskRemoved(rootIntent: Intent?) {
        super.onTaskRemoved(rootIntent)
        routeTimer.cancelTimer()
        this.stopSelf()
    }

    override fun onDestroy() {
        super.onDestroy()
        routeTimer.cancelTimer()
        this.stopSelf()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        routeTimer.startTimer()
        return super.onStartCommand(intent, flags, startId)
    }
}