package com.paulconroy.bustrack.interfaces

import com.paulconroy.bustrack.adapter.RouteListAdapter
import com.paulconroy.bustrack.adapter.StopListAdapter
import com.paulconroy.bustrack.model.Route

/**
 * Created by paulconroy on 26/09/2017.
 */
interface PagerListener {

    fun proceed()

    fun hideKeyboard()

    fun fetchResults()

    fun setStopNumber(stopNumber: String)

    fun getAdapter(): RouteListAdapter

    fun getFavouriteAdapter(): StopListAdapter

    fun setListener(fragmentListener: FragmentListener)

    fun setDirection(direction: String)

    fun getDirectionRoutes() : ArrayList<Route>

    fun displayDialog()

    fun getRoutes() : ArrayList<Route>

    fun checkDirection() : String

    fun goBack()

    fun addFavouriteToRealm(customName: String)

    fun removeFavouriteFromRealm()

    fun checkIfStopIsFavourite() : Boolean

    fun isFavourite(): Boolean
}